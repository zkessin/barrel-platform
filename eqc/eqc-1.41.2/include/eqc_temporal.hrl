
-import(eqc_temporal, [
    at/2, from_timed_list/1, product/2, map/2, flatmap/2, filter/2, last/1,
    all_past/1, all_past/2, all_future/1, all_future/2,
    union/2, unions/1, empty/0, constant/1,
    any_past/2, any_past/1, any_future/2, any_future/1, negation/1, negation/2,
    complement/1, is_false/1, is_true/1, is_subrelation/2, implies/2,
    stateful/3, subtract/2, shift/2, slice/3, ret/1, bind/2, completion/1,
    restrict/2, split_at/2, partition/2, range/1, domain_size/1, set/1,
    elems/1, intersection/2, coarsen/2]).

